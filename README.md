# yaml2json

`yaml2json` is a CLI tool to convert YAML input to JSON output.

Intended for use in environments where reading YAML files is required and
heavyweight dependencies like Ruby or Python are undesirable.


## Installation

TODO


## Usage

Note: This doesn't actually work yet!

```
$ echo '---\nfoo:bar\nbaz:4' | yaml2json
{"foo":"bar","baz":4}
```

```
$ yaml2json myfile.yaml
{"examples":["complete",2]}
```
