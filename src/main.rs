use std::env;
use std::fs::File;
use std::io::prelude::*;

fn filename_to_contents(filename: &String) -> String {
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents)
    .expect("something went wrong reading the file");
    return contents;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let slice = &args[1..];
    println!("{:?}", slice);

    for filename in slice.iter() {
        let contents = filename_to_contents(filename);
        println!("Filename: {}", filename);
        println!("Contents:\n{}", contents);
    }
}
