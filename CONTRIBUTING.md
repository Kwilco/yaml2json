If you would like to contribute, please open an issue or submit a merge request.

If you don't receive a response within a week, please email me.

All submissions to this project must be MIT licensed. This will be assumed
implicitly upon submission of a merge request.